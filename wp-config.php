<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-silect');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V1!IOJteTtm+Cqwh*-_,02nqeC:FP[w@CzGe?Jg85/~D5D)6}(_Al.=FK9!TAd>r');
define('SECURE_AUTH_KEY',  'O:q]<^:o>9pq$ND@lBkM@/ThRwAF8TYNYQ&6&o9~K7dy;,4_I[-Z|&&zf|#[N+Ig');
define('LOGGED_IN_KEY',    '@dk1,$(~9q*m7{6H@D?t.N&9~}aWfT)WV|(09]$_b;2k|bBg91.+tc]hWEpHPhG-');
define('NONCE_KEY',        'CS?,qLl$jg}FuTr64FCNES6DwFsGB`%S.sxjx*In>/BI`Q.fV:5,hb<XVz-0 *+@');
define('AUTH_SALT',        'Q=%^=Fe3~<o]nK~Dr5+qnOBsZ3w66%qsR~lW=s1-)vIV>kDa4nzL&el_4I:%dk+k');
define('SECURE_AUTH_SALT', 'X sTyiclN+j+<6B=RQgx@hQ!nV,>W:|?@seTMU>EvrvWrq+|:[7&}4~-=wo@*oAy');
define('LOGGED_IN_SALT',   '?7U3^h)@/(7QP4a+$e4nabMemRxZ--_29>vwwyzxr,|J+P2m:z=s[Po2QT>4p7.D');
define('NONCE_SALT',       '1rOZ#gb-,iJ~?,6hT`+GexWO`p)Ts:Z*Pv2eQ6#5*>@)|Z;M7:z%{+o*Wa,&RJ~]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
